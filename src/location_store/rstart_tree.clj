(ns location-store.rstart-tree
  (:require [location-store.helpers :refer [min-index except-nth uuid-gen! id-map make-groups]]
            [location-store.geometry.rectangle :as geom]))

(defn empty-node []
  {:id       (uuid-gen!)
   :children {}
   :mbr      (geom/rectangle 0 0 0 0)
   :type     :node})

(defn poi [id mbr]
  {:id   id
   :mbr  mbr
   :type :poi})

(defn random-poi []
  (let [max 10000
        x-min  (rand-int max)
        x-max  (+ x-min (rand-int (- max x-min)))
        y-min  (rand-int max)
        y-max  (+ y-min (rand-int (- max y-min)))]
    (poi (uuid-gen!) (geom/rectangle x-min x-max y-min y-max))))

(defn poi? [item]
  (= :poi (:type item)))

(defn leaf? [node]
  (or (empty? (:children node))
      (poi? (first (vals (:children node))))))

(defn entities [node]
  (if (poi? node)
    [node]
    (apply concat (map entities (vals (:children node))))))

(defn height [root]
  (cond
    (poi? root)  0
    (leaf? root) 1
    true         (inc (apply max (map height (vals (:children root)))))))

(defn- overfill? [root max-child]
  (> (count (:children root)) max-child))

(defn valid? [root max-child min-child]
  (if (poi? root)
    true
    (and (not (overfill? root max-child))
         ;; Also need to check if very leaf is at the same level.
         ;; (= 1 (count (distinct (map height (vals :children root)))))
         (every? #(valid? % max-child min-child) (vals (:children root))))))

(defn- node-group-mbr [node-group]
  (apply geom/mbr (map :mbr node-group)))

(defn- overlap-enlargement [target-rect other-rects new-rect]
  "Calculates the increase in overlap area after adding a new rectangle to a set of rectangles"
  (let [old-overlap (reduce + (map #(geom/overlap-area target-rect %) other-rects))
        new-overlap (reduce + (map #(geom/overlap-area (geom/mbr target-rect new-rect) %) other-rects))]
    (- new-overlap old-overlap)))

(defn enlargement [target-rect new-rect]
  (- (geom/area (geom/mbr target-rect new-rect))
     (geom/area target-rect)))

(defn- choose-least-overlap [nodes new-rect]
  (let [ith-others-pair (map-indexed
                         (fn [i n] [n (except-nth i nodes)])
                         nodes)]
    (first
     (apply min-key
            (fn [[n others]] (overlap-enlargement (:mbr n) (map :mbr others) new-rect))
            ith-others-pair))))

(defn- choose-least-enlargement [nodes new-rect]
  (apply min-key #(enlargement (:mbr %) new-rect) nodes))

(defn- overlap-value [[group1 group2]]
  (geom/overlap-area (node-group-mbr group1)
                     (node-group-mbr group2)))

(defn- group-margin [group]
  (geom/margin (apply geom/mbr (map :mbr group))))

(defn- margin-value [[group1 group2]]
  (+ (group-margin group1)
     (group-margin group2)))

(defn- choose-split [nodes max-size min-size]
  (let [axises             [:x-min :x-max :y-min :y-max]
        sort-nodes-by-axis (fn [axis] (sort-by #(axis (:mbr %)) nodes))
        groups-by-axis     (fn [axis] (make-groups (sort-nodes-by-axis axis) max-size min-size))
        axis-margin-value  (fn [axis] (apply min (map margin-value (groups-by-axis axis))))
        ;; Choose the axis which has lowest margin-value of all the groups which are formed along that axis.
        choosen-axis       (apply min-key axis-margin-value axises)]
    (apply min-key overlap-value (groups-by-axis choosen-axis))))

(defn- split [node max-child min-child]
  (let [[group1 group2] (choose-split (vals (:children node)) max-child min-child)
        node1           (assoc node
                               :children (id-map group1)
                               :mbr      (node-group-mbr group1))
        node2           (assoc (empty-node)
                               :children (id-map group2)
                               :mbr      (node-group-mbr group2))]
    [node1 node2]))

(defn- choose-sub-tree [node poi]
  (if (leaf? node)
    node
    (if (leaf? (first (:children node)))
      ;; TODO: Skipping the tie breaking step in choose-least-overlap and choose-least enlargement
      ;; I guess there won't be many ties and it won't make much difference.
      (choose-least-overlap (vals (:children node)) (:mbr poi))
      (choose-least-enlargement (vals (:children node)) (:mbr poi)))))

(defn- insert-subtrees [node subtrees]
  (let [children (merge (:children node) (id-map subtrees))
        new-mbr  (node-group-mbr (vals children))
        new-node (assoc node
                        :children children
                        :mbr new-mbr)]
    new-node))

(defn- insert-internal [node poi max-child min-child]
  "Returns a vector of trees after inserting a point of interest at a node.
Vector contains a single tree if no split happens at the root level"
  (let [modified-subtrees (if (leaf? node)
                            [poi]
                            (insert-internal (choose-sub-tree node poi) poi max-child min-child))
        new-node          (insert-subtrees
                           node
                           modified-subtrees)]
    (if (overfill? new-node max-child)
      (split new-node max-child min-child)
      [new-node])))

(defn insert
  "Insert a point of interest (poi) into an rstart-tree."
  [root poi max-child min-child]
  (let [subtrees (insert-internal root poi max-child min-child)]
    (if (= 1 (count subtrees))
      (first subtrees)
      ;; If the split happens on the root level then we need to create a new root
      (insert-subtrees (empty-node) subtrees))))
