(ns location-store.db
  (:require [location-store.rstart-tree :as tree]
            [location-store.spec :as spec]
            [location-store.config :as config]
            [clojure.spec.alpha :as s]))

(defn- read-from-file []
  (when (.exists (clojure.java.io/file (config/data-file)))
    (let [file-db (read-string (slurp (config/data-file)))]
      (when (s/valid? ::spec/node file-db)
        file-db))))

(defonce ^:private db (ref (if-let [file-db (read-from-file)]
                             file-db
                             (tree/empty-node))))

(defn- write-to-file []
  (if (s/valid? ::spec/node @db)
    (spit (config/data-file) (str @db))
    (throw (Exception. (s/explain ::spec/node @db)))))

(defn store [poi]
  (dosync
   (let [new-tree (tree/insert @db poi (config/max-child) (config/min-child))]
     (ref-set db new-tree)))
  (write-to-file))

(defn retrive [{:keys [point radius]}]
  nil)
