(ns location-store.config)

(defonce profile (atom :dev))

(defn port []
  (if (= @profile :dev)
    7000
    7001))

(defn max-child []
  10)

(defn min-child []
  3)

(defn data-file []
  (if (= @profile :dev)
    "data.clj"
    "test-data.clj"))
