(ns location-store.route
  (:require [location-store.middleware :as mw]
            [location-store.handler :as handler]
            [bidi.ring :refer [make-handler]]))

(defn routes []
  ["/" {""   {:post handler/store
              :get  handler/retrive}
        true (fn [request]     {:status 404 :body {:error "URL not found"}})}])

(defn handler []
  (->
   (routes)
   make-handler
   mw/wrap-json-request
   mw/wrap-json-response))
