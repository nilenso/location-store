(ns location-store.spec
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.gen.alpha :as gen]
            [location-store.geometry.spec :as geom-spec]
            [location-store.rstart-tree :as tree]))

(s/def ::id string?)
(s/def ::mbr ::geom-spec/rectangle)
(s/def ::type #{:poi :node})
(s/def ::poi (s/and
              (s/keys :req-un [::id ::mbr ::type])
              (fn [{:keys [type]}] (= :poi type))))
(s/def ::children (s/or :leaf (s/map-of ::id ::poi)
                        :non-leaf (s/map-of ::id ::node)))
(s/def ::node (s/and
               (s/keys :req-un [::id ::mbr ::children ::type])
               (fn [{:keys [type]}] (= :node type))))

(s/def ::store-params (s/keys :req-un [::id ::mbr]))

(s/def ::radius integer?)
(s/def ::retrive-params (s/keys ::geom-spec/point ::radius))

(s/fdef tree/insert
        :args (s/and
               (s/cat :root ::node :poi ::poi :max-child integer? :min-child integer?)
               #(<= (:min-child %) (:max-child %)))
        :ret (s/coll-of ::node))

(stest/instrument `tree/insert)
