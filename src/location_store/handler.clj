(ns location-store.handler
  (:require [location-store.spec :as spec]
            [location-store.db :as db]
            [location-store.geometry.rectangle :as geom]
            [location-store.rstart-tree :as tree]
            [clojure.spec.alpha :as s]))

(defn store [{:keys [body]}]
  (if (s/valid? ::spec/store-params body)
    (let [poi (assoc body :type :poi)]
      (db/store poi)
      {:status 200
       :body   {:id (:id poi)}})
    {:status 400
     :body   {:error (s/explain-str ::spec/store-params body)}}))

(defn retrive [{:keys [body]}]
  (if (s/valid? ::spec/retrive-params body)
    (let [retrived-pois (db/retrive body)]
      {:status 200
       :body   retrived-pois})
    {:status 400
     :body   {:error (s/explain-str ::spec/retrive-params body)}}))
