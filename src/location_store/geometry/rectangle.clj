(ns location-store.geometry.rectangle)

(defn rectangle [x-min x-max y-min y-max]
  {:x-min x-min
   :x-max x-max
   :y-min y-min
   :y-max y-max})

(defn area [{:keys [x-min x-max y-min y-max]}]
  (* (- x-max x-min)
     (- y-max y-min)))

(defn mbr [& rects]
  "Calculated the minimum bounding rectangle enclosing a set of rectangles"
  (rectangle
   (apply min (map :x-min rects))
   (apply max (map :x-max rects))
   (apply min (map :y-min rects))
   (apply max (map :y-max rects))))

(defn- interval-overlap
  [[a1 a2] [b1 b2]]
  (cond
    (<= a1 b1 a2) (min (- a2 b1) (- b2 b1))
    (<= b1 a1 a2) (min (- b2 a1) (- a2 a1))
    true          0))

(defn overlap-area
  [r1 r2]
  (* (interval-overlap [(:x-min r1) (:x-max r1)] [(:x-min r2) (:x-max r2)])
     (interval-overlap [(:y-min r1) (:y-max r1)] [(:y-min r2) (:y-max r2)])))

(defn margin
  [rect]
  (let [x-side (- (:x-max rect) (:x-min rect))
        y-side (- (:y-max rect) (:y-min rect))]
    (+ (* 2 x-side)
       (* 2 y-side))))
