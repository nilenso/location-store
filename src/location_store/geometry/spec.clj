(ns location-store.geometry.spec
  (:require [clojure.spec.alpha :as s]))

(s/def ::coordinate integer?)
(s/def ::point (s/coll-of ::coordinate :count 2))
(s/def ::x-min ::coordinate)
(s/def ::x-max ::coordinate)
(s/def ::y-min ::coordinate)
(s/def ::y-max ::coordinate)
(s/def ::rectangle (s/and (s/keys :req-un [::x-min ::x-max ::y-min ::y-max])
                          (fn [{:keys [x-min x-max y-min y-max]}]
                            (and (<= x-min x-max) (<= y-min y-max)))))
