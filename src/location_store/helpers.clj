(ns location-store.helpers)

(defn min-index [coll]
  (second (min-key first (interleave coll (range)))))

(defn except-nth [n coll]
  (concat (take (dec n) coll)
          (drop n coll)))

(defn uuid-gen! []
  (str (java.util.UUID/randomUUID)))

(defn id-map [item-list]
  (reduce (fn [m n] (assoc m (:id n) n))
          {}
          item-list))

(defn make-groups [sorted-nodes max-size min-size]
  (map (fn [k] (split-at k sorted-nodes))
       (range min-size (inc (- (count sorted-nodes) min-size)))))
