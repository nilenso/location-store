(ns location-store.core
  (:gen-class)
  (:require [location-store.config :as config]
            [location-store.route :as route]
            [location-store.spec :as spec]
            [org.httpkit.server :as httpkit]))

(defonce server (atom nil))

(defn start-server! []
  (reset! server (httpkit/run-server (route/handler) {:port (config/port)}))
  (println "Server started at port: "  (config/port)))

(defn stop-server! []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))

(defn restart-server! []
  (stop-server!)
  (start-server!))

(defn -main
  [& args]
  (start-server!))
