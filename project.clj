(defproject location-store "0.1.0-SNAPSHOT"
  :description "Storing in a queryable manner, the locations of things."
  :url "https://gitlab.com/nilenso/location-store"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.json "0.2.6"]
                 [http-kit "2.2.0"]
                 [bidi "2.1.3"]]
  :main ^:skip-aot location-store.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
