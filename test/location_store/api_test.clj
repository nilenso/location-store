(ns location-store.api-test
  (:require  [clojure.test :refer :all]
             [location-store.fixtures :as fixtures]
             [location-store.config :as config]
             [location-store.core :as core]
             [org.httpkit.client :as http]
             [clojure.data.json :as json]))

(use-fixtures :once fixtures/with-test-config fixtures/start-stop-server fixtures/delete-db)

(defn api-end-point [] (format "http://localhost:%d/" (config/port)))

(defn parse-body [body]
  (json/read-str body :key-fn keyword))

(deftest store-test
  (let [id       "test-point-of-interest1"
        poi      {:id id :mbr {:x-min 0 :x-max 1 :y-min 0 :y-max 1}}
        response @(http/post (api-end-point) {:body (json/write-str poi)})]
    (is (= 200 (:status response)))
    (is (= id (:id (parse-body (:body response)))))))
