(ns location-store.rstart-tree-test
  (:require [location-store.rstart-tree :refer :all]
            [location-store.helpers :refer [uuid-gen!]]
            [location-store.geometry.rectangle :as geom]
            [clojure.test :refer :all]))

;; Test if the tree remains valid
;; Test if all the pois are present
;; Test the height of the tree
(deftest insert-test
  (testing "the tree remains valid after random inserts"
    (let [max-child 7
          min-child 3
          n         200
          root      (reduce #(insert %1 %2 max-child min-child)
                            (empty-node)
                            (repeatedly n random-poi))]
      (is (= n (count (entities root))))
      (is (valid? root max-child min-child)))))
