(ns location-store.fixtures
  (:require  [clojure.test :as t]
             [location-store.config :as config]
             [location-store.core :refer [start-server! stop-server!]]))

(defn with-test-config [f]
  (swap! config/profile :test)
  (f)
  (swap! config/profile :dev))

(defn start-stop-server [f]
  (start-server!)
  (f)
  (stop-server!))

(defn delete-db [f]
  (f)
  (clojure.java.io/delete-file (config/data-file)))
