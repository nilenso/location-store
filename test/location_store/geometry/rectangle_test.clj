(ns location-store.geometry.rectangle-test
  (:require [location-store.geometry.rectangle :refer :all]
            [clojure.test :refer :all]))


(deftest area-test
  (is (= 6 (area (rectangle 0 3 0 2)))))

(deftest overlap-area-test
  (is (= 0 (overlap-area (rectangle 0 1 0 1) (rectangle 1 3 1 3))))
  (is (= 1 (overlap-area (rectangle 0 1 0 1) (rectangle 0 4 0 4))))
  (is (= 1 (overlap-area (rectangle 0 3 0 3) (rectangle 2 4 2 4)))))

(deftest margin-test
  (is (= 0 (margin (rectangle 1 1 1 1))))
  (is (= 10 (margin (rectangle 0 3 0 2)))))


(deftest mbr-test
  (is (= (rectangle 1 5 1 7)
         (mbr
          (rectangle 1 2 2 2)
          (rectangle 3 5 6 7)
          (rectangle 3 4 1 3)))))
