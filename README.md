# location-store

Storing in a queryable manner, the locations of things.

## License

Copyright © 2018 Nilenso Software LLP.

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
